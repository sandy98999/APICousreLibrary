﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourseLibrary.Client.Models
{
    public class OrderCourseViewModel
    {
        public string Address { get; private set; } = string.Empty;

        public OrderCourseViewModel(string address)
        {
            Address = address;
        }
    }
}
