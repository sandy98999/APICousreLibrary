﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CourseLibrary.Client.Models;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Newtonsoft.Json;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace CourseLibrary.Client.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IHttpClientFactory _clientFactory;

        public HomeController(ILogger<HomeController> logger,IHttpClientFactory clientFactory)
        {
            _logger = logger ?? 
                throw new ArgumentNullException(nameof(logger));
            _clientFactory = clientFactory ?? 
                throw new ArgumentNullException(nameof(clientFactory));
        }

        public async Task<IActionResult> Index()
        {
            await WriteIdentityToken();

            var httpClient = _clientFactory.CreateClient("APIClient");

            var request = new HttpRequestMessage(
                HttpMethod.Get,
                "/api/Authors/");

            var response = await httpClient.SendAsync(
                request, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                var responseStream = await response.Content.ReadAsStringAsync();
                var author = JsonConvert.DeserializeObject<List<Author>>(responseStream);

                return View(author);
            }
            else if(response.StatusCode == HttpStatusCode.Unauthorized ||
                    response.StatusCode == HttpStatusCode.Forbidden)
            {
                return RedirectToAction("AccessDenied", "Authorization");
            }

            throw new Exception("Problem accessing API");

        }

        [Authorize(Policy = "CanOrder")]
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task WriteIdentityToken()
        {
            var identityToken = await HttpContext
                .GetTokenAsync(OpenIdConnectParameterNames.IdToken);

            Debug.WriteLine($"Identity Token is: {identityToken}");

            foreach (var claim in User.Claims)
            {
                Debug.WriteLine($"Claim Type: {claim.Type} - Claim Value: {claim.Value}");
            }
        }

        [Authorize(Roles = "PaidUser")]
        public async Task<IActionResult> OrderCourse()
        {
            var _idpClient = _clientFactory.CreateClient("IDPClient");

            var metaData = await _idpClient.GetDiscoveryDocumentAsync();

            if (metaData.IsError)
            {
                throw new Exception(
                    "Problem accessing discovery endpoint",
                    metaData.Exception
                    );
            }

            var accessToken = await HttpContext
                .GetTokenAsync(OpenIdConnectParameterNames.AccessToken);

            var userInfo = await _idpClient.GetUserInfoAsync(
                new UserInfoRequest
                {
                    Address = metaData.UserInfoEndpoint,
                    Token = accessToken
                });

            if (userInfo.IsError)
            {
                throw new Exception(
                    "Problem accessing UserInfo Endpoint",
                    userInfo.Exception
                    );
            }

            var address = userInfo.Claims
                .FirstOrDefault(x => x.Type == "address")?.Value;

            return View(new OrderCourseViewModel(address));
        }

        public async Task LogOut()
        {
            var client = _clientFactory.CreateClient("IDPClient");

            var discoveryEndpoint = await client.GetDiscoveryDocumentAsync();

            if (discoveryEndpoint.IsError)
            {
                throw new Exception(discoveryEndpoint.Issuer);
            }

            var accessTokenRevoke = await client.RevokeTokenAsync(
                new TokenRevocationRequest
                {
                    Address = discoveryEndpoint.RevocationEndpoint,
                    ClientId = "courselibrary",
                    ClientSecret = "secret",
                    Token = await HttpContext.GetTokenAsync(OpenIdConnectParameterNames.AccessToken)
                });

            if (accessTokenRevoke.IsError)
            {
                throw new Exception(accessTokenRevoke.Error);
            }

            var refreshTokenRevoke = await client.RevokeTokenAsync(
                new TokenRevocationRequest
                {
                    Address = discoveryEndpoint.RevocationEndpoint,
                    ClientId = "courselibrary",
                    ClientSecret = "secret",
                    Token = await HttpContext.GetTokenAsync(OpenIdConnectParameterNames.RefreshToken)
                });

            if (refreshTokenRevoke.IsError)
            {
                throw new Exception(refreshTokenRevoke.Error);
            }

            //await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            //await HttpContext.SignOutAsync(OpenIdConnectDefaults.AuthenticationScheme);
        }
    }
}
