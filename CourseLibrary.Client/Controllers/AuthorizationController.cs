﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace CourseLibrary.Client.Controllers
{
    public class AuthorizationController : Controller
    {
        public async Task<IActionResult> AccessDenied()
        {
            return View();
        }
    }
}
