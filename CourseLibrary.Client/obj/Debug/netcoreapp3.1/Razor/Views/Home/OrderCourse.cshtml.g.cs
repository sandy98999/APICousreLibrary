#pragma checksum "G:\Visual Studio codes\CourseLibrary\CourseLibrary.Client\Views\Home\OrderCourse.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8c8598ba74dc1158f6562e91ec869bcd153839c2"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_OrderCourse), @"mvc.1.0.view", @"/Views/Home/OrderCourse.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "G:\Visual Studio codes\CourseLibrary\CourseLibrary.Client\Views\_ViewImports.cshtml"
using CourseLibrary.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "G:\Visual Studio codes\CourseLibrary\CourseLibrary.Client\Views\_ViewImports.cshtml"
using CourseLibrary.Client.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8c8598ba74dc1158f6562e91ec869bcd153839c2", @"/Views/Home/OrderCourse.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6ab8a922b86dfdcf34c47719c8f43d1d70937676", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_OrderCourse : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<CourseLibrary.Client.Models.OrderCourseViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n<div class=\"container\">\r\n    <div class=\"text\">This is the address in our record: </div>\r\n    <div class=\"text text-info\">");
#nullable restore
#line 5 "G:\Visual Studio codes\CourseLibrary\CourseLibrary.Client\Views\Home\OrderCourse.cshtml"
                           Write(Model.Address);

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>\r\n</div>\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<CourseLibrary.Client.Models.OrderCourseViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
