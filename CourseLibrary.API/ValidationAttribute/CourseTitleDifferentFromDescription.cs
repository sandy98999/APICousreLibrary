﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using CourseLibrary.API.Models;

namespace CourseLibrary.API.ValidationAttribute
{
    public class CourseTitleDifferentFromDescription : System.ComponentModel.DataAnnotations.ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var course = (CourseForManipulationDto) validationContext.ObjectInstance;

            if (course.Title == course.Description)
            {
                return new ValidationResult("Title should be different from description",new[]{nameof(CourseForManipulationDto)});
            }

            return ValidationResult.Success;
        }
    }
}
