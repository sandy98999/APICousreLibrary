using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using CourseLibrary.API.AuthorizationPolicy;
using CourseLibrary.API.DbContexts;
using CourseLibrary.API.Helper;
using CourseLibrary.API.Models;
using CourseLibrary.API.Services;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace CourseLibrary.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(setupAction =>
           {
               setupAction.Conventions.Add(new ApiExplorerIgnores()); //Swagger does not allow route on Action, ConfirmEmail has to be done through URL so ignoring that action
               setupAction.ReturnHttpNotAcceptable = true;
               setupAction.CacheProfiles.Add("240SecondCache",
                   new CacheProfile { Duration = 240});
           })
            .AddNewtonsoftJson(setUp =>
            {
                setUp.SerializerSettings.ContractResolver =
                    new CamelCasePropertyNamesContractResolver();
            })
            .AddXmlDataContractSerializerFormatters();

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddResponseCaching();

            Extentions.Extention.IdentityExtention(services);

            services.AddScoped<ICourseLibraryRepository, CourseLibraryRepository>();

            services.AddScoped<IUserService, UserService>();

           services.AddDbContext<CourseLibraryContext>(options =>
           {
               options.UseSqlServer(
                   Configuration.GetConnectionString("DefaultConnection"));
           });

           services.AddTransient<IEmailService, EmailService>();

           Extentions.Extention.SwaggerExtention(services);

           services.AddHttpContextAccessor();
           services.AddTransient<IAuthorizationHandler, UserRestrictionHandler>();

           // Below Authentication was for in application authentication with identity and JWT
           //services.AddAuthentication(auth =>
           //    {
           //        auth.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
           //        auth.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
           //    })
           //    .AddJwtBearer(jwt =>
           //    {
           //        jwt.TokenValidationParameters = new TokenValidationParameters
           //        {
           //            ValidateIssuer = true,
           //            ValidateAudience = true,
           //            ValidIssuer = Configuration["JwtToken:Issuer"],
           //            ValidAudience = Configuration["JwtToken:Audience"],
           //            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtToken:Key"])),
           //            ValidateIssuerSigningKey = true,
           //            RequireExpirationTime = true
           //        };
           //    });

           //Authentication for OpenId Connect
           services.AddAuthentication(auth=>
               {
                   auth.DefaultAuthenticateScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
                   auth.DefaultChallengeScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
               })
               .AddIdentityServerAuthentication(options =>
               {
                   options.Authority = "https://localhost:52874/";
                   options.ApiName = "courselibraryapi";
                   options.ApiSecret = "apisecret";
               });

           services.AddAuthorization(options =>
           {
               options.AddPolicy("RestrictParticularUser",
                   policyBuilder =>
                   {
                       policyBuilder.AddRequirements(new RestrictUserRequirement());
                   });
           });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler(appBuilder =>
                    {
                        appBuilder.Run(async context =>
                        {
                            context.Response.StatusCode = 500;
                            await context.Response.WriteAsync("Error occurred");
                        });
                    }
                );
            }

            app.UseSwagger();

            app.UseSwaggerUI(ui => ui.SwaggerEndpoint("/swagger/v1/swagger.json", "Course Library Api"));

            app.UseResponseCaching();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
