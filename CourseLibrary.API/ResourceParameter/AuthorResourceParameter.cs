﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourseLibrary.API.ResourceParameter
{
    public class AuthorResourceParameter
    {
        public string MainCategory { get; set; }
        public string SearchQuery { get; set; }
    }
}
