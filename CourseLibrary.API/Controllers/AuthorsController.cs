﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CourseLibrary.API.Entities;
using CourseLibrary.API.Helper;
using CourseLibrary.API.Models;
using CourseLibrary.API.ResourceParameter;
using CourseLibrary.API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CourseLibrary.API.Controllers
{
    [ApiController]
    [Route("api/authors")]
    [Authorize]
    public class AuthorsController:ControllerBase
    {
        private readonly ICourseLibraryRepository _courseLibraryRepository;
        private readonly IMapper _mapper;

        public AuthorsController(ICourseLibraryRepository courseLibraryRepository,IMapper mapper)
        {
            _courseLibraryRepository = courseLibraryRepository??
                throw new ArgumentNullException(nameof(courseLibraryRepository));

            _mapper = mapper ??
                      throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet]
        [HttpHead]
        //[Authorize(Roles = "User")]
        //[Authorize(Policy = "RestrictParticularUser")]
        public IActionResult GetAuthors([FromQuery]AuthorResourceParameter authorResourceParameter)
        {
            var authorFromRepo = _courseLibraryRepository.GetAuthors(authorResourceParameter);
            return Ok(_mapper.Map<IEnumerable<AuthorDto>>(authorFromRepo));
        }

        [HttpGet("{authorId:guid}",Name = "GetAuthor")]
        public IActionResult GetAuthor(Guid authorId)
        {
            var authorFromRepo = _courseLibraryRepository.GetAuthor(authorId);

            if (authorFromRepo == null)
                return NotFound();

            return Ok(_mapper.Map<AuthorDto>(authorFromRepo));
        }

        [HttpPost]
        public ActionResult<AuthorDto> CreateAuthor(AuthorForCreationDto authorForCreationDto)
        {
            var mapped = _mapper.Map<Author>(authorForCreationDto);
            _courseLibraryRepository.AddAuthor(mapped);
            _courseLibraryRepository.Save();

            var authorToReturn = _mapper.Map<AuthorDto>(mapped);

            return CreatedAtRoute("GetAuthor", new {authorId = authorToReturn.Id}, authorToReturn);
        }

        [HttpOptions]
        public IActionResult GetAuthorOptions()
        {
            Response.Headers.Add("Allow","GET,POST,OPTIONS");
            return Ok();
        }
    }
}
