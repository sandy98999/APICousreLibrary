﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CourseLibrary.API.Models;
using CourseLibrary.API.Services;
using Microsoft.AspNetCore.Mvc;

namespace CourseLibrary.API.Controllers
{
    [ApiController]
    [Route("api/Account")]
    public class AccountController : ControllerBase
    {
        private readonly IUserService _userService;

        public AccountController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("Register")]
        public async Task<IActionResult> Register(UserRegistrationViewModel registrationViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Some properties are invalid");
            }

            var result = await _userService.RegisterUserAsync(registrationViewModel);

            if (result.IsSuccess)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpPost("{token}/{email}")]
        [Route("ConfirmEmail")]
        public async Task<IActionResult> ConfirmEmail([FromQuery]string token, [FromQuery]string email)
        {
            if (token == null || email == null)
                return BadRequest("Token or email can not be empty");

            var result = await _userService.ConfirmEmail(email, token);
            if (result == "Success")
                return Ok(result);

            return BadRequest(result);
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid email or password");

            var login = await _userService.LoginUserAsync(loginViewModel);

            if (login.IsSuccess)
                return Ok(login);

            return BadRequest();
        }
    }
}
