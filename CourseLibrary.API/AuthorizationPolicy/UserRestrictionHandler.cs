﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace CourseLibrary.API.AuthorizationPolicy
{
    public class UserRestrictionHandler : AuthorizationHandler<RestrictUserRequirement>
    {
        private readonly IHttpContextAccessor _httpContext;

        public UserRestrictionHandler(IHttpContextAccessor httpContext)
        {
            _httpContext = httpContext;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, RestrictUserRequirement requirement)
        {
            //var routeId = _httpContext.HttpContext.GetRouteValue("id").ToString();

            var owner = context.User.Claims.FirstOrDefault(x => x.Type == "sub")?.Value;

            //if (owner == "818727")
            //{
            //    context.Fail();
            //    return Task.CompletedTask;
            //}

            context.Succeed(requirement);
            return Task.CompletedTask;
        }
    }
}
