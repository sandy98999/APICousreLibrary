﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;
using CourseLibrary.API.Models;
using Microsoft.AspNetCore.Mvc;

namespace CourseLibrary.API.Services
{
    public interface IUserService
    {
        Task<UserManagerResponse> RegisterUserAsync(UserRegistrationViewModel registrationViewModel);

        Task<string> ConfirmEmail(string email, string token);

        Task<string> SendConfirmationEmailAsync(ApplicationIdentityUser applicationIdentityUser);

        Task AddRoleAsync(RolesEnum role);

        Task<UserManagerResponse> LoginUserAsync(LoginViewModel login);

        Tuple<string,DateTime> CreateToken(ApplicationIdentityUser user,IEnumerable<string> roles);
    }
}
