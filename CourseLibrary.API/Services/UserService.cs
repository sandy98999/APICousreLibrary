﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CourseLibrary.API.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace CourseLibrary.API.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<ApplicationIdentityUser> _userManager;
        private readonly IEmailService _emailService;
        private readonly IMapper _mapper;
        private readonly ILogger<UserService> _logger;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IConfiguration _configuration;

        public UserService(UserManager<ApplicationIdentityUser> userManager,
            IEmailService emailService,
            IMapper mapper,
            ILogger<UserService> logger,
            RoleManager<IdentityRole> roleManager,
            IConfiguration configuration)
        {
            _userManager = userManager ??
                throw new ArgumentNullException(nameof(userManager));
            _emailService = emailService ??
                throw new ArgumentNullException(nameof(emailService));
            _mapper = mapper ??
                throw new ArgumentNullException(nameof(mapper));
            _logger = logger ??
                throw new ArgumentNullException(nameof(logger));
            _roleManager = roleManager ??
                throw new ArgumentNullException(nameof(roleManager));
            _configuration = configuration;
        }

        public async Task<string> ConfirmEmail(string email, string token)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
                return "User Not Found";

            var decodedToken = WebEncoders.Base64UrlDecode(token);
            var normalToken = Encoding.UTF8.GetString(decodedToken);

            var result = await _userManager.ConfirmEmailAsync(user, normalToken);
            if (result.Succeeded)
                return "Success";

            return result.ToString();
        }

        public async Task<UserManagerResponse> RegisterUserAsync(UserRegistrationViewModel registrationViewModel)
        {
            if (registrationViewModel == null)
                throw new NotImplementedException();

            try
            {
                var identityUser = _mapper.Map<ApplicationIdentityUser>(registrationViewModel);

                var result = await _userManager.CreateAsync(identityUser, registrationViewModel.Password);

                if (result.Succeeded)
                {
                    var sendEmail = await SendConfirmationEmailAsync(identityUser);

                    await AddRoleAsync(RolesEnum.User);

                    await _userManager.AddToRoleAsync(identityUser, RolesEnum.User.ToString());

                    return new UserManagerResponse
                    {
                        Message = "User created successfully, Please verify your email to log in.",
                        IsSuccess = true
                    };
                }

                return new UserManagerResponse
                {
                    Message = "User could not be created",
                    IsSuccess = false,
                    Errors = result.Errors.Select(r => r.Description)
                };
            }
            catch (Exception ex)
            {
                return new UserManagerResponse
                {
                    Message = "Something went wrong",
                    IsSuccess = false,
                    Errors = ex.Message.Select(s => s.ToString())
                };
            }
        }

        public async Task<string> SendConfirmationEmailAsync(ApplicationIdentityUser applicationIdentityUser)
        {
            var token = await _userManager.GenerateEmailConfirmationTokenAsync(applicationIdentityUser);

            var encodedToken = Encoding.UTF8.GetBytes(token);
            var validToken = WebEncoders.Base64UrlEncode(encodedToken);

            var url = $"{_configuration["ApplicationUrl"]}/Account/ConfirmEmail?" +
                      $"email={applicationIdentityUser.Email}&token={validToken}";

            _logger.LogInformation($"Token URL is {url}");

            try
            {
                await _emailService.SendEmailAsync(applicationIdentityUser.Email,
                    "Confirm Email",
                    $"<p>Please confirm your email by <a href='{url}>Click Here</a></p>'");
            }
            catch (Exception ex)
            {
                _logger.LogWarning($"Email Service is not working, confirmation email could not be sent for user {applicationIdentityUser.Email}.");
                return ex.Message;
            }

            return "Success";
        }

        public async Task AddRoleAsync(RolesEnum role)
        {
            bool userRoleExists = await _roleManager.RoleExistsAsync(role.ToString());
            if (!userRoleExists)
            {
                _logger.LogInformation($"Adding role : {role.ToString()}");
                await _roleManager.CreateAsync(new IdentityRole(role.ToString()));
            }
        }

        public async Task<UserManagerResponse> LoginUserAsync(LoginViewModel login)
        {
            var user = await _userManager.FindByEmailAsync(login.Email);

            if (user != null && await _userManager.CheckPasswordAsync(user, login.Password))
            {
                _logger.LogInformation($"User validated {user.Email}");

                var roles = await _userManager.GetRolesAsync(user);
                var token = CreateToken(user, roles ); 

                return new UserManagerResponse
                {
                    Message = token.Item1,
                    IsSuccess = true,
                    Expires = token.Item2
                };
            }

            _logger.LogInformation($"Invalid username or password for {login.Email}");

            return new UserManagerResponse
            {
                Message = "Invalid email or password",
                IsSuccess = false
            };
        }

        public Tuple<string,DateTime> CreateToken(ApplicationIdentityUser user, IEnumerable<string> roles)
        {
            var claim = new List<Claim>();

            claim.Add(new Claim(ClaimTypes.Email,user.Email));
            claim.Add(new Claim(ClaimTypes.Name,$"{user.FirstName} {user.LastName}"));

            foreach (var role in roles)
            {
                claim.Add(new Claim(ClaimTypes.Role,role));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtToken:Key"]));

            var token = new JwtSecurityToken(
                    issuer: _configuration["JwtToken:Issuer"],
                    audience: _configuration["JwtToken:Audience"],
                    claims: claim,
                    expires: DateTime.Now.AddMinutes(Convert.ToInt32(_configuration["JwtToken:Expires"])),
                    signingCredentials: new SigningCredentials(key,SecurityAlgorithms.HmacSha512)
                );

            var tokenString = new JwtSecurityTokenHandler().WriteToken(token);

            Tuple<string, DateTime> tokenTuple = Tuple.Create(tokenString, token.ValidTo);

            return tokenTuple;
        }
    }
}
