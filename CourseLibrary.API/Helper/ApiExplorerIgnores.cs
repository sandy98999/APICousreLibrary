﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ApplicationModels;

namespace CourseLibrary.API.Helper
{
    public class ApiExplorerIgnores : IActionModelConvention
    {
        public void Apply(ActionModel action)
        {
            //if (action.Controller.ControllerName.Equals("Account"))
            //    action.ApiExplorer.IsVisible = false;

            if (action.ActionName.Equals("ConfirmEmail"))
                action.ApiExplorer.IsVisible = false;
        }
    }
}
