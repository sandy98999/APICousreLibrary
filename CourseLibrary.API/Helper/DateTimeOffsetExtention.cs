﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace CourseLibrary.API.Helper
{
    public static class DateTimeOffsetExtentions
    {
        public static int GetCurrentAge(this DateTimeOffset offset)
        {
            var currentDate = DateTime.UtcNow;
            int age = currentDate.Year - offset.Year;

            if (currentDate < offset.AddYears(age))
                age--;

            return age;
        }
    }
}
