﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CourseLibrary.API.Models;

namespace CourseLibrary.API.Profiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            CreateMap<UserRegistrationViewModel,ApplicationIdentityUser>()
                .ForMember(
                    m=>m.UserName,
                    opt=>opt.MapFrom(src=>src.Email)
                    );
        }
    }
}
